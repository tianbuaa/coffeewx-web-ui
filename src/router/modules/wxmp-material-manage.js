/** When your routing table is too long, you can split it into small modules**/

// 素材管理

import Layout from '@/views/layout/Layout'

const wxmpMaterialManage = {
  path: '/wxmpMaterialManage',
  component: Layout,
  alwaysShow: true,
  name: '素材管理',
  redirect: 'noredirect',
  meta: {
    title: '素材管理',
    icon: 'table',
    resources: 'wxmpMaterialManage'
  },
  children: [
    {
      path: 'textTemplateList',
      component: () => import('@/views/wxmp/material-manage/text-template-manage/textTemplateList'),
      name: '文本管理',
      meta: { title: '文本管理', icon: 'documentation', noCache: true, resources: 'textTemplateList' }
    },
    {
      path: 'newsTemplateList',
      component: () => import('@/views/wxmp/material-manage/news-template-manage/newsTemplateList'),
      name: '图文管理',
      meta: { title: '图文管理', icon: 'icon-news', noCache: true, resources: 'newsTemplateList' }
    },
    {
      path: 'newsTemplateForm/:id(\\d+)',
      component: () => import('@/views/wxmp/material-manage/news-template-manage/newsTemplateForm'),
      name: '编辑图文',
      meta: { title: '编辑图文', noCache: true, resources: 'newsTemplateForm' },
      hidden: true
    }
  ]
}

export default wxmpMaterialManage
